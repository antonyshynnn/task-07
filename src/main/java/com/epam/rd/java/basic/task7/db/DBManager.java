package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static Connection connection = null;

	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance =  new DBManager();
		return  instance;
	}

	private DBManager() {
		try {
			//connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db","root","12345");
			connection = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> userList = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement("SELECT id, login FROM users", Statement.RETURN_GENERATED_KEYS)
		) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				User user = User.createUser(resultSet.getString("login"));
				user.setId(resultSet.getInt("id"));
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		try (PreparedStatement statement = connection.prepareStatement("INSERT INTO users VALUES (DEFAULT, ?)", Statement.RETURN_GENERATED_KEYS);
		) {
			statement.setString(1, user.getLogin());
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("No rows affected");
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					user.setId(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			}

		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
		}

		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (PreparedStatement statement = connection.prepareStatement("DELETE FROM	users WHERE (?)", Statement.RETURN_GENERATED_KEYS);
		) {
			for (User user : users) {
				statement.setString(1, user.getLogin());
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User user = User.createUser(login);
		try (PreparedStatement statement = connection.prepareStatement("SELECT id, login FROM users WHERE login = (?)")) {
			statement.setString(1, login);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				user.setId(resultSet.getInt("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = Team.createTeam(name);
		try (PreparedStatement statement = connection.prepareStatement("SELECT id, name FROM teams WHERE name = (?)")) {
			statement.setString(1, name);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				team.setId(resultSet.getInt("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement("SELECT id, name FROM teams", Statement.RETURN_GENERATED_KEYS);
		) {
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Team team = Team.createTeam(resultSet.getString("name"));
				team.setId(resultSet.getInt("id"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (PreparedStatement statement = connection.prepareStatement("INSERT INTO teams VALUES (DEFAULT, ?)", Statement.RETURN_GENERATED_KEYS);
		) {
			statement.setString(1, team.getName());
			int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				throw new SQLException("No rows affected");
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					team.setId(generatedKeys.getInt(1));
				} else {
					throw new SQLException("Creating team failed, no ID obtained.");
				}
			}
		} catch (SQLException sqlException) {
			sqlException.printStackTrace();
		}

		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (user == null) throw new DBException("", new NullPointerException());
		try {
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try (PreparedStatement statement = connection.prepareStatement("INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)")) {
			for (Team team : teams) {
				if (team == null) {
					connection.rollback();
					throw new DBException("", new NullPointerException());
				}
				statement.setInt(1, user.getId());
				statement.setInt(2, team.getId());
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException("", new NullPointerException());
		}
		try {
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeams = new ArrayList<>();
		try (PreparedStatement statement = connection.prepareStatement("SELECT user_id, team_id FROM users_teams WHERE user_id = (?)")) {
			statement.setInt(1, user.getId());
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				int teamId = resultSet.getInt("team_id");
				Team team = Team.createTeam(getTeamNameById(teamId));
				team.setId(teamId);
				userTeams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userTeams;
	}

	public String getTeamNameById(int teamId) {
		String name = null;
		try (PreparedStatement statement = connection.prepareStatement("SELECT name FROM teams WHERE  id = (?)")){
			statement.setInt(1, teamId);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				name = resultSet.getString("name");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return name;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (PreparedStatement statement = connection.prepareStatement("DELETE FROM teams WHERE name = (?)")) {
			statement.setString(1, team.getName());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		try (PreparedStatement statement = connection.prepareStatement("UPDATE teams SET name = ? WHERE id = ?");
		) {
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
